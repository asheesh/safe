/**
 * The app navigator (formerly "AppNavigator" and "MainNavigator") is used for the primary
 * navigation flows of your app.
 * Generally speaking, it will contain an auth flow (registration, login, forgot password)
 * and a "main" flow which the user will use once logged in.
 */
import React, { useEffect, useState } from "react"
import { useColorScheme, View, Alert } from "react-native"
import { NavigationContainer, DefaultTheme, DarkTheme } from "@react-navigation/native"
import { createNativeStackNavigator } from "@react-navigation/native-stack"
import { AddRecordScreen, FolderScreen, HomeScreen, RecordScreen, SignupScreen, SplashScreen} from "../screens"
import { navigationRef } from "./navigation-utilities"
import { LoggedUserModel, useStores } from "../models"
import { observer } from "mobx-react-lite"
import { createDrawerNavigator, DrawerContentScrollView, DrawerItem, DrawerItemList } from '@react-navigation/drawer';
import { Icon, Text } from "react-native-elements"
import { color } from "../theme/color"
import EncryptedStorage from 'react-native-encrypted-storage';
import { SettingsScreen } from "../screens/settings/settings-screen"
import * as RNFS from 'react-native-fs';
import Toast from "react-native-root-toast"
import { virgilCrypto } from "react-native-virgil-crypto"
import DeviceInfo from 'react-native-device-info';

export type NavigatorParamList = {
  home: undefined,
  signup: undefined,

  folders: {},
  records: {folderId: 0, folderName: ""},
  addRecord: {folderId: 0, folderName: "", recordId: 0,},
  settings:undefined
}

const Stack = createNativeStackNavigator<NavigatorParamList>()

const AuthStack = (props) => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName={props.initialRouteName}
    >
          <Stack.Screen name="signup" component={SignupScreen}
          options={{
            headerShown: false
          }} />
          <Stack.Screen name="home" component={HomeScreen}
          options={{
            headerShown: false
          }} />
          
    </Stack.Navigator>
  )
}
const AppStack = ({ navigation, route }) => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: true,
        
      }}
      initialRouteName={route.params.initialRouteName}
    >
          <Stack.Screen name="folders" component={FolderScreen} 
            options={{
              title: " Safe",
              headerLeft: () => (
                <Icon name="navicon" size={20} color={color.primary} type="font-awesome" onPress={() => {navigation.toggleDrawer();} }/>
              ),
            }}/>
          <Stack.Screen name="records" component={RecordScreen} /> 
          <Stack.Screen name="addRecord" component={AddRecordScreen} 
            options={{
              headerTitle: "Add Record",
              presentation: "transparentModal"
            }}/>
            <Stack.Screen name="settings" component={SettingsScreen} options={{
              title: " Settings",
              headerLeft: () => (
                <Icon name="navicon" size={20} color={color.primary} type="font-awesome" onPress={() => {navigation.toggleDrawer();} }/>
              ),
            }} /> 
    </Stack.Navigator>
  )
}

const AppDrawerContent = (props) => {
  const { folderStore, recordStore } = useStores()

  const { loggedUser } = useStores()
  const [name, setName] = useState("")
  const logout = () => {
    loggedUser.saveLoggedUser(LoggedUserModel.create({}))
  }
  useEffect(() => {
    EncryptedStorage.getItem("name").then((name) => {
      setName(name)
    });
  }, [])

  const importFile = () => {
    var dataPath = RNFS.DownloadDirectoryPath + "/safe/safedata"
    var publicKeyPath = RNFS.DownloadDirectoryPath + "/safe/publickey"
    var privateKeyPath = RNFS.DownloadDirectoryPath + "/safe/privatekey"

    RNFS.readFile(publicKeyPath, 'utf8').then(async publicKey => {
      await EncryptedStorage.setItem("secure_key_public", publicKey);
      RNFS.readFile(privateKeyPath, 'utf8').then(async privateKey => {
        await EncryptedStorage.setItem("secure_key_private", privateKey);
        RNFS.readFile(dataPath, 'utf8').then(async res => {
          var key = await EncryptedStorage.getItem("secure_key_private");
          const privateKey = virgilCrypto.importPrivateKey(key);
          const decryptedData = virgilCrypto.decrypt(res, privateKey);
          var data = JSON.parse(decryptedData.toString('utf8'));
          folderStore.saveFolders(data.folderStore.folders)
          recordStore.saveRecords(data.recordStore.records)
          loggedUser.saveLoggedUser(LoggedUserModel.create({token:"fake_token"}))
          Toast.show('import successfully', {
            duration: Toast.durations.LONG,
            position: Toast.positions.BOTTOM,
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0
          });
        })
        .catch(err => {
          if(err.message.indexOf("No such file or directory") >= 0){
            loggedUser.saveLoggedUser(LoggedUserModel.create({token:"fake_token"}))
          }
            console.log(err.message, err.code);
        });
      })
      .catch(err => {
        Toast.show('Unable to import', {
          duration: Toast.durations.LONG,
          position: Toast.positions.BOTTOM,
          shadow: true,
          animation: true,
          hideOnPress: true,
          delay: 0
        });
      });
    })
    .catch(err => {
      Toast.show('Unable to import', {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0
      });
    });
     
    RNFS.readFile(dataPath, 'utf8').then(async res => {
      var key = await EncryptedStorage.getItem("secure_key_private");
      const privateKey = virgilCrypto.importPrivateKey(key);
      const decryptedData = virgilCrypto.decrypt(res, privateKey);
      var data = JSON.parse(decryptedData.toString('utf8'));
      folderStore.saveFolders(data.folderStore.folders)
      recordStore.saveRecords(data.recordStore.records)
      loggedUser.saveLoggedUser(LoggedUserModel.create({token:"fake_token"}))
    })
    .catch(err => {
      if(err.message.indexOf("No such file or directory") >= 0){
        loggedUser.saveLoggedUser(LoggedUserModel.create({token:"fake_token"}))
      }
        console.log(err.message, err.code);
    });
  }

  const confirmImport = () => {
    Alert.alert(
      "Confirm",
      "Importing will clear any currently saved data. To start importing keep the exported 'safe' folder in downloads folder?",
      [
        {
          text: "Cancel",
          style: "cancel"
        },
        { text: "OK", onPress: () => importFile() }
      ]
    );
  }

  const exportFile = async () => {
    var srcPath = RNFS.DocumentDirectoryPath + "/safedata"
    var destPath = RNFS.DownloadDirectoryPath + "/safe/safedata"
    var publicKeyPath = RNFS.DownloadDirectoryPath + "/safe/publickey"
    var privateKeyPath = RNFS.DownloadDirectoryPath + "/safe/privatekey"
    await RNFS.mkdir(RNFS.DownloadDirectoryPath + "/safe");
    RNFS.copyFile(srcPath, destPath).then(async () => {
      var publicKey = await EncryptedStorage.getItem("secure_key_public");
      var privateKey = await EncryptedStorage.getItem("secure_key_private");
      RNFS.writeFile(publicKeyPath, publicKey, 'utf8').then(res => {
        RNFS.writeFile(privateKeyPath, privateKey, 'utf8').then(res => {
          Toast.show('Exported to Downloads', {
            duration: Toast.durations.LONG,
            position: Toast.positions.BOTTOM,
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0
          });
        })
        .catch(err => {
          Toast.show('Unable to export', {
            duration: Toast.durations.LONG,
            position: Toast.positions.BOTTOM,
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0
          });
        });
      })
      .catch(err => {
        Toast.show('Unable to export', {
          duration: Toast.durations.LONG,
          position: Toast.positions.BOTTOM,
          shadow: true,
          animation: true,
          hideOnPress: true,
          delay: 0
        });
      });
    })
    .catch(err => {
      Toast.show('Unable to export', {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0
      });
    });
  }

  const confirmIExport = () => {
    Alert.alert(
      "Confirm",
      "3 Files will be exported to downloads folder in a folder named 'safe'. Keep the files in a secure place?",
      [
        {
          text: "Cancel",
          style: "cancel"
        },
        { text: "OK", onPress: () => exportFile() }
      ]
    );
  }

  return (
    <DrawerContentScrollView {...props} contentContainerStyle={{flex: 1}}>
      <View>
        <View style={{marginBottom: 30, flexDirection: "row", alignItems: "center", justifyContent: "center"}}>
          <Icon name="safe" type="material-community" size={60}  color={color.primary}/>
          <Text style={{color: color.primary, fontSize: 50, marginLeft: 5}}>Safe</Text>
        </View>
        <View style={{flexDirection: "row", paddingHorizontal: 15, flexGrow: 1, justifyContent:"space-between" }}>
          <Text style={{color: color.palette.blue}}>Hello, {name}</Text>
          <Text style={{color: color.palette.blue}}>{"v" + DeviceInfo.getVersion()}</Text>
        </View>
        <View style={{ borderBottomColor: color.primary,borderBottomWidth: 1,width:"90%" , alignSelf:"center", marginBottom: 15}}/>
        <DrawerItemList {...props}/>
        <DrawerItem label="Export File" onPress={()=>{confirmIExport()}} />
        <DrawerItem label="Import File" onPress={()=>{confirmImport()}} />
        <View style={{ borderBottomColor: color.primary,borderBottomWidth: 1,width:"90%" , alignSelf:"center", marginTop: 200}}/>
      </View>
      <DrawerItem
      style={{marginBottom: 20}}
      label="Logout"
      icon={() => <Icon name="power-off" size={30} color={color.primary} type="font-awesome" />}
      onPress={()=>{
        logout()
              }}
    />
     
   </DrawerContentScrollView>
  )
}
   

const Drawer = createDrawerNavigator();

const AppDrawer = ()  =>{
  return (
    <Drawer.Navigator initialRouteName="Folders" drawerContent={props => <AppDrawerContent {...props} />} screenOptions={{headerShown: false}}>
      <Drawer.Screen name="Folders" component={AppStack} initialParams={{ initialRouteName: "folders" }}/>
      <Drawer.Screen name="Settings" component={AppStack} initialParams={{ initialRouteName: "settings" }}/>
    </Drawer.Navigator>
  );
}


interface NavigationProps extends Partial<React.ComponentProps<typeof NavigationContainer>> {}

export const AppNavigator = observer((props: NavigationProps) => {
  const colorScheme = useColorScheme()
  const { loggedUser } = useStores()
  const [isLoading, setIsLoading] = useState(true)
  const [isRegistered, setIsRegistered] = useState(false)
  
  useEffect(() => {
    EncryptedStorage.getItem("pass_key").then((passKey) => {
      setIsRegistered(passKey ? true : false)
    });
  }, [])
  
  if(isLoading){
    setTimeout(() => {
      setIsLoading(false)
    }, 2000)
  }
  
  if(isLoading){
    return  (
      <SplashScreen></SplashScreen> 
    )
  }

  return (
    <NavigationContainer
      ref={navigationRef}
      theme={colorScheme === "dark" ? DarkTheme : DefaultTheme}
      {...props}
    >
      {loggedUser.isLogged ? <AppDrawer /> : <AuthStack initialRouteName={isRegistered ? "home" : "signup"}/>}
    </NavigationContainer>
  )
})

AppNavigator.displayName = "AppNavigator"

/**
 * A list of routes from which we're allowed to leave the app when
 * the user presses the back button on Android.
 *
 * Anything not on this list will be a standard `back` action in
 * react-navigation.
 *
 * `canExit` is used in ./app/app.tsx in the `useBackButtonHandler` hook.
 */
const exitRoutes = ["folders"]
export const canExit = (routeName: string) => exitRoutes.includes(routeName)
