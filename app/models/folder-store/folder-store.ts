import { getParent, Instance, SnapshotOut, types } from "mobx-state-tree"
import { FolderModel, FolderSnapshot } from "../folder/folder"
import { RootStore } from "../root-store/root-store"


/**
 * Model description here for TypeScript hints.
 */
export const FolderStoreModel = types
  .model("FolderStore")
  .props({
    folders: types.optional(types.array(FolderModel), []),
  })
  .views((self) => {
    return {
      
    }
  }) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    addFolder: (folder: FolderSnapshot) => {
      self.folders.push(folder)
    },
    saveFolders: (folders: FolderSnapshot[]) => {
      self.folders.replace(folders)
    },
    updateFolder: (folderToUpdate: FolderSnapshot) => {
      let folder = self.folders.find((x) => x.id == folderToUpdate.id)
      folder.name = folderToUpdate.name
    },
    deleteFolder: (folderId: number) => {
      let folder = self.folders.find((x) => x.id == folderId)
      self.folders.remove(folder)
      getParent<RootStore>(self).recordStore.deleteRecordsOfFolder(folderId)
    }
  }))
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

type FolderStoreType = Instance<typeof FolderStoreModel>
export interface FolderStore extends FolderStoreType {}
type FolderStoreSnapshotType = SnapshotOut<typeof FolderStoreModel>
export interface FolderStoreSnapshot extends FolderStoreSnapshotType {}
export const createFolderStoreDefaultModel = () => types.optional(FolderStoreModel, {})
