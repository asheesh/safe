import { Instance, SnapshotOut, types } from "mobx-state-tree"

/**
 * Model description here for TypeScript hints.
 */
export const FolderModel = types
  .model("Folder")
  .props({
    id: types.identifierNumber,
    name: types.maybe(types.string),
  })
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

type FolderType = Instance<typeof FolderModel>
export interface Folder extends FolderType {}
type FolderSnapshotType = SnapshotOut<typeof FolderModel>
export interface FolderSnapshot extends FolderSnapshotType {}
export const createFolderDefaultModel = () => types.optional(FolderModel, {})
