import { Instance, SnapshotOut, types } from "mobx-state-tree"
import { RecordModel, RecordSnapshot } from "../record/record"

/**
 * Model description here for TypeScript hints.
 */
export const RecordStoreModel = types
  .model("RecordStore")
  .props({
    records: types.optional(types.array(RecordModel), []),
  })
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    addRecord: (newRecord: RecordSnapshot) => {
      self.records.push(newRecord)
    },
    saveRecords: (records: RecordSnapshot[]) => {
      self.records.replace(records)
    },
    updateRecord: (newRecord: RecordSnapshot) => {
      let record = self.records.find((x) => x.id == newRecord.id)
      record.name = newRecord.name
      record.username = newRecord.username
      record.password = newRecord.password
      record.notes = newRecord.notes
    },
    findById: (id: number) => {
      return self.records.find((x) => x.id == id)
    },
    deleteRecord: (recordId: number) => {
      let record = self.records.find((x) => x.id == recordId)
      self.records.remove(record)
    },
    deleteRecordsOfFolder: (folderId: number) => {
      let recordsToRemove = self.records.filter(item => item.folderId == folderId)
      recordsToRemove.forEach(record => {
        self.records.remove(record)
      })
    },
    findByFolderId: (id: number) => {
      return self.records.filter((x) => x.folderId == id)
    }
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

type RecordStoreType = Instance<typeof RecordStoreModel>
export interface RecordStore extends RecordStoreType {}
type RecordStoreSnapshotType = SnapshotOut<typeof RecordStoreModel>
export interface RecordStoreSnapshot extends RecordStoreSnapshotType {}
export const createRecordStoreDefaultModel = () => types.optional(RecordStoreModel, {})
