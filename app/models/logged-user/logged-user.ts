import { Instance, SnapshotOut, types } from "mobx-state-tree"

/**
 * Model description here for TypeScript hints.
 */
export const LoggedUserModel = types
  .model("LoggedUser")
  .props({
    token: types.maybe(types.string)
  })
  .views((self) => {
    return {
      get isLogged() {
        return self.token != null;
      }
    }
    
  }) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    saveLoggedUser: (loggedUser: LoggedUserSnapshot) => {
      self.token = loggedUser.token
    },
  }))
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

type LoggedUserType = Instance<typeof LoggedUserModel>
export interface LoggedUser extends LoggedUserType {}
type LoggedUserSnapshotType = SnapshotOut<typeof LoggedUserModel>
export interface LoggedUserSnapshot extends LoggedUserSnapshotType {}
export const createLoggedUserDefaultModel = () => types.optional(LoggedUserModel, {})
