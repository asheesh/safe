import { Instance, SnapshotOut, types } from "mobx-state-tree"
import { FolderStoreModel } from "../folder-store/folder-store"
import { LoggedUserModel } from "../logged-user/logged-user"
import { RecordStoreModel } from "../record-store/record-store"

/**
 * A RootStore model.
 */
// prettier-ignore
export const RootStoreModel = types.model("RootStore").props({
  loggedUser: types.optional(LoggedUserModel, {} as any),
  folderStore: types.optional(FolderStoreModel, {} as any),
  recordStore: types.optional(RecordStoreModel, {} as any)
})

/**
 * The RootStore instance.
 */
export interface RootStore extends Instance<typeof RootStoreModel> {}

/**
 * The data of a RootStore.
 */
export interface RootStoreSnapshot extends SnapshotOut<typeof RootStoreModel> {}
