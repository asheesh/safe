import { Instance, SnapshotOut, types } from "mobx-state-tree"

/**
 * Model description here for TypeScript hints.
 */
export const RecordModel = types
  .model("Record")
  .props({
    id: types.identifierNumber,
    name: types.string,
    username: types.maybeNull(types.string),
    password: types.maybeNull(types.string),
    notes: types.maybeNull(types.string),
    folderId: types.number
  })
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

type RecordType = Instance<typeof RecordModel>
export interface Record extends RecordType {}
type RecordSnapshotType = SnapshotOut<typeof RecordModel>
export interface RecordSnapshot extends RecordSnapshotType {}
export const createRecordDefaultModel = () => types.optional(RecordModel, {})
