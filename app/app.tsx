/**
 * Welcome to the main entry point of the app. In this file, we'll
 * be kicking off our app.
 *
 * Most of this file is boilerplate and you shouldn't need to modify
 * it very often. But take some time to look through and understand
 * what is going on here.
 *
 * The app navigation resides in ./app/navigators, so head over there
 * if you're interested in adding screens and navigators.
 */
 import 'react-native-gesture-handler';

import "./i18n"
import "./utils/ignore-warnings"
import React, { useState, useEffect } from "react"
import { SafeAreaProvider, initialWindowMetrics } from "react-native-safe-area-context"
import { initFonts } from "./theme/fonts" // expo
import * as storage from "./utils/storage"
import { useBackButtonHandler, AppNavigator, canExit, useNavigationPersistence } from "./navigators"
import { LoggedUserModel, RootStore, RootStoreProvider, setupRootStore } from "./models"
import { ToggleStorybook } from "../storybook/toggle-storybook"
import { ErrorBoundary } from "./screens/error/error-boundary"
import { AppState , PermissionsAndroid} from "react-native"
import { RootSiblingParent } from 'react-native-root-siblings';
import EncryptedStorage from 'react-native-encrypted-storage';
import { virgilCrypto } from 'react-native-virgil-crypto';

// This puts screens in a native ViewController or Activity. If you want fully native
// stack navigation, use `createNativeStackNavigator` in place of `createStackNavigator`:
// https://github.com/kmagiera/react-native-screens#using-native-stack-navigator

export const NAVIGATION_PERSISTENCE_KEY = "NAVIGATION_STATE"

/**
 * This is the root component of our app.
 */
function App() {
  const [rootStore, setRootStore] = useState<RootStore | undefined>(undefined)

  useBackButtonHandler(canExit)
  const {
    initialNavigationState,
    onNavigationStateChange,
    isRestored: isNavigationStateRestored,
  } = useNavigationPersistence(storage, NAVIGATION_PERSISTENCE_KEY)

  let loggedUser1
  // Kick off initial async loading actions, like loading fonts and RootStore
  useEffect(() => {
    ;(async () => {
      await initFonts() // expo
      setupRootStore().then((rootStore) => {
        setRootStore(rootStore)
        const {loggedUser} = rootStore
        loggedUser1 = loggedUser
        setupKeyAndStore()
        requestStoragePermission()
      })
    })()
  }, [])

  const requestStoragePermission = async () =>{ console.log("1")
    try {
      PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE).then(async function(res){
        if(!res){
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            {
              'title': 'Safe',
              'message': 'wants access to use import/export feature',
              buttonNeutral: "Ask Me Later",
              buttonPositive: "OK"
            }
          )
        }
      })
    } catch (err) {
    }
  }

  const setupKeyAndStore = async () => {
    var secureKeyPrivate = await EncryptedStorage.getItem("secure_key_private");
    var secureKeyPublic = await EncryptedStorage.getItem("secure_key_public");
    if(!secureKeyPrivate){
      const keyPair = virgilCrypto.generateKeys()
      const privateKey = virgilCrypto.exportPrivateKey(keyPair.privateKey).toString('base64');
      const publicKey = virgilCrypto.exportPublicKey(keyPair.publicKey).toString('base64');
      await EncryptedStorage.setItem("secure_key_private", privateKey);
      await EncryptedStorage.setItem("secure_key_public", publicKey);
    }
  }
  
  const onStateChange = (nextAppState) => {
    if (
      nextAppState.match(/inactive|background/) 
    ) {
      loggedUser1.saveLoggedUser(LoggedUserModel.create({}))
    }
  }

  useEffect(() => {
    AppState.addEventListener("change", onStateChange);

    return () => {
      AppState.removeEventListener("change", onStateChange)
    };
  }, []);

  // Before we show the app, we have to wait for our state to be ready.
  // In the meantime, don't render anything. This will be the background
  // color set in native by rootView's background color.
  // In iOS: application:didFinishLaunchingWithOptions:
  // In Android: https://stackoverflow.com/a/45838109/204044
  // You can replace with your own loading component if you wish.
  if (!rootStore || !isNavigationStateRestored) return null

  // otherwise, we're ready to render the app
  return (
    <ToggleStorybook>
      <RootStoreProvider value={rootStore}>
        <RootSiblingParent>
          <SafeAreaProvider initialMetrics={initialWindowMetrics}>
            <ErrorBoundary catchErrors={"always"}>
              <AppNavigator
                initialState={initialNavigationState}
                onStateChange={onNavigationStateChange}
              />
            </ErrorBoundary>
          </SafeAreaProvider>
        </RootSiblingParent>
      </RootStoreProvider>
    </ToggleStorybook>
  )
}

export default App
