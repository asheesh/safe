import React, { useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { StyleSheet, View, Text } from "react-native"
import { Screen } from "../../components"
import { color } from "../../theme"
import { ListItem, Switch } from "react-native-elements"
import EncryptedStorage from "react-native-encrypted-storage"

const Styles = StyleSheet.create({
  root:{
    flex: 1,
    justifyContent: "flex-start", 
  }
})

export const SettingsScreen = observer(function SettingsScreen() {
  const [useBiometrics, setUseBiometrics] = useState(false);

  useEffect(() => {
    EncryptedStorage.getItem("use_biometrics").then((useBiometrics) => {
      setUseBiometrics(useBiometrics == "true" ? true : false)
    });
  }, [])

  const toggleUseBiometrics = (value) => { 
    EncryptedStorage.setItem("use_biometrics", value ? "true" : "false").then(() => {
      setUseBiometrics(value)
    });
  }

  return (
    <Screen style={Styles.root} preset="scroll">
      <ListItem  topDivider key={1} >
        <ListItem.Content>
          <ListItem.Title>Use Biometric</ListItem.Title>
        </ListItem.Content>
        <Switch style={{alignItems:"flex-end"}}
            value={useBiometrics}
            onValueChange={(value) => toggleUseBiometrics(value)} />
      </ListItem>
    </Screen>
  )
})

