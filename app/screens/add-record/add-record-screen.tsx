import React, { FC, useEffect, useState } from "react"
import { Button, StyleSheet, TextInput, Text, View } from "react-native"
import { observer } from "mobx-react-lite"
import { GradientBackground, Screen } from "../../components"
import { color } from "../../theme"
import { StackScreenProps } from "@react-navigation/stack"
import { NavigatorParamList } from "../../navigators"
import { Controller, useForm } from "react-hook-form"
import { Icon, Input } from "react-native-elements"
import { RecordModel } from "../../models/record/record"
import { useStores } from "../../models"
import Toast from "react-native-root-toast"

const styles = StyleSheet.create({
  root:{
    flex:1,
  },
  screen:{
    backgroundColor: color.transparent,
    elevation:10
  },
})

export const AddRecordScreen: FC<StackScreenProps<NavigatorParamList, "addRecord">> = observer(({ route, navigation }) => {
  const { recordStore } = useStores()
  const { records } = recordStore
  const recordId = route.params.recordId
  const folderId = route.params.folderId

  const [hidePassWord, setHidePassWord] = useState(true)

  let record = {name: "", username: "", password: "", notes: ""}
  if(recordId != 0){
    record = recordStore.findById(recordId)
  }

  useEffect(() => {
    navigation.setOptions({
      title: recordId == 0 ? "Add Record" : record.name,
      headerRight: () => (
        <Button onPress={handleSubmit(onSubmit)} title="Save" />
      )
    })
    
  }, [])

  const { control, handleSubmit, formState: { errors } } = useForm();
  const onSubmit = data => {
    recordId == 0 ? addRecord(data) : updateRecord(data)
    navigation.navigate("records", {folderId: route.params.folderId, folderName: route.params.folderName})
    Toast.show('Created', {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0
    });
  }

  const addRecord = (data) => {
    var newRecordId = records.length > 0 ? Math.max(...records.map(x => x.id)) + 1 : 1
    recordStore.addRecord(RecordModel.create({id: newRecordId, name: data.name, username: data.username, password: data.password, notes: data.notes, folderId: folderId}))
  }

  const updateRecord = (data) => {
    recordStore.updateRecord(RecordModel.create({id: recordId, name: data.name, username: data.username, password: data.password, notes: data.notes, folderId: folderId}))
  }

  return (
    <View style={styles.root}>
      <GradientBackground colors={[color.palette.lighterGrey, color.palette.lighterGrey]} />
      <Screen preset="scroll">
        <View style={{paddingHorizontal: 15, paddingVertical: 10}}>
          <Controller
              control={control}
              rules={{
              required: true,
              }}
              render={({ field: { onChange, onBlur, value } }) => (
                <Input
                  errorMessage={errors.email && "This is required."}
                  label="Name"
                  leftIcon={<Icon name="tag" size={20} color={color.primary} type="font-awesome" />}
                  onBlur={onBlur}
                  onChangeText={onChange}
                  value={value} textAlign="left" />
              )}
              name="name"
              defaultValue={record.name}
            />
            <Controller
              control={control}
              rules={{
              }}
              render={({ field: { onChange, onBlur, value } }) => (
                <Input
                  errorMessage={errors.email && "This is required."}
                  label="Username"
                  leftIcon={<Icon name="user" size={20} color={color.primary} type="font-awesome" />}
                  onBlur={onBlur}
                  onChangeText={onChange}
                  value={value} textAlign="left" />
              )}
              name="username"
              defaultValue={record.username}
            />
            <Controller
              control={control}
              rules={{
              }}
              render={({ field: { onChange, onBlur, value } }) => (
                <Input
                  errorMessage={errors.email && "This is required."}
                  label="Password"
                  leftIcon={<Icon name="key" size={20} color={color.primary} type="font-awesome" />}
                  secureTextEntry={hidePassWord}
                  onBlur={onBlur}
                  onChangeText={onChange}
                  rightIcon={<Icon name={!hidePassWord ? "eye-slash": "eye"} size={20} color={color.primary} type="font-awesome" onPress={() => setHidePassWord(!hidePassWord)}/>}
                  value={value} textAlign="left" />
              )}
              name="password"
              defaultValue={record.password}
            />
            <Text>Notes </Text>
            <Controller
              control={control}
              rules={{
              }}
              render={({ field: { onChange, onBlur, value } }) => (
                <TextInput
                  multiline={true}
                  //errorMessage={errors.email && "This is required."}
                  onBlur={onBlur}
                  onChangeText={onChange}
                  value={value}
                  style={{ height:250, textAlignVertical: 'top', backgroundColor: color.palette.white, borderRadius:15, borderWidth: 2, borderColor: color.palette.lightGrey, marginTop:20, padding:15}}/>
              )}
              name="notes"
              defaultValue={record.notes}
            />
        </View>
      </Screen>
    </View>
    
  )
})
