export * from "./error/error-boundary"
// export other screens here
export * from "./folder/folder-screen"
export * from "./home/home-screen"
export * from "./add-record/add-record-screen"
export * from "./record/record-screen"
export * from "./signup/signup-screen"
export * from "./splash/splash-screen"
export * from "./settings/settings-screen"
