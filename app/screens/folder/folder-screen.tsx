import React, { FC, useState } from 'react';
import { observer } from "mobx-react-lite"
import { Alert, StyleSheet, View } from "react-native"
import { Screen, GradientBackground } from "../../components"
import { color } from "../../theme"
import { FolderModel, useStores } from "../../models"
import { Button, ListItem, Overlay, Icon, Input, FAB } from "react-native-elements"
import { NavigatorParamList } from '../../navigators';
import { StackScreenProps } from '@react-navigation/stack';
import { useRef } from 'react';
import Toast from 'react-native-root-toast';
import { useEffect } from 'react';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';

const Styles = StyleSheet.create({
  root:{
    flex: 1,
    padding:5
  },
  screen:{
    backgroundColor: color.transparent,
    elevation:10
  },
  itemContainer:{
    alignItems: "center",
    flexDirection: "row",
    padding: 10
  },
  folderName:{
    marginLeft: 10
  },
  addFolderContainer:{
    width: "90%"
  },
  addFolderTitle:{
    color:color.palette.deepPurple
  },
  addFolderBodyContainer:{
  }
  
})



export const FolderScreen: FC<StackScreenProps<NavigatorParamList, "folders">> = observer(({ navigation }) => {
  // Pull in navigation via hook
  // const navigation = useNavigation()

  const { folderStore } = useStores()
  const { folders } = folderStore
  const [folderToUpdate, setFolderToUpdate] = useState({id: 0, name: '', error: ''});
  const newFolderInput = useRef(null);

  const [isAddFolderFormVisible, setIsAddFolderFormVisible] = useState(false);

  const showFolderOverlay = (item) => {
    setFolderToUpdate({error: "", name: item.name, id: item.id})
    setIsAddFolderFormVisible(true);
  };

  const validateNewFolderName = (text):string =>  {
    let error = null
    if(text == null || text == ""){
      error = "cannot be empty"
    }
    return error
  }

  const validateAndSetNewFolderName = (text) => {
    let errorMsg = validateNewFolderName(text)
    if(errorMsg != null){
      setFolderToUpdate({error: errorMsg, name: "", id: folderToUpdate.id})
      newFolderInput.current.shake()
      newFolderInput.current.setNativeProps({inputContainerStyle: {borderColor: color.palette.orange}})
    }else{
      setFolderToUpdate({error: null, name: text, id: folderToUpdate.id})
      newFolderInput.current.setNativeProps({inputContainerStyle: {}})
    }
  }

  const createNewFolder = () => {
    if(folderToUpdate.error == null){
      var newFolderId = folders.length > 0 ? Math.max(...folders.map(x => x.id)) + 1 : 1
      folderStore.addFolder(FolderModel.create({id: newFolderId, name: folderToUpdate.name}))
      setIsAddFolderFormVisible(false)
      Toast.show('Created', {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0
      });
    }
  }

  const updateFolder = () => {
    if(folderToUpdate.error == null){
      folderStore.updateFolder(FolderModel.create({id: folderToUpdate.id, name: folderToUpdate.name}))
      setIsAddFolderFormVisible(false)
      Toast.show('Updated', {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0
      });
    }
  }
  
  useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <View style={{flexDirection: "row", width: 80, justifyContent: "space-between", alignItems: "center"}}>
        </View>
      )
    })
  }, [])

  const confirmAndDeleteFolder = (folderId) => {
    Alert.alert(
      "Confirm",
      "Are you sure?",
      [
        {
          text: "Cancel",
          style: "cancel"
        },
        { text: "OK", onPress: () => deleteFolder(folderId) }
      ]
    );
  }

  const deleteFolder = (folderId) => {
    folderStore.deleteFolder(folderId)
    Toast.show('Deleted', {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0
    });
  }

  const navigateToRecords = (item) => { console.log("Sdfsdf")
    navigation.navigate("records", {folderId: item.id, folderName: item.name});
  }
  
  const renderItem = ({ item }) => (
    <ListItem bottomDivider topDivider key={item.id} >
      <ListItem.Content >
        <ListItem.Title>{ item.name }</ListItem.Title>
      </ListItem.Content>
      <Icon name="pencil" color={color.primary} onPress={() => {showFolderOverlay(item)}} type="font-awesome"></Icon>
      <Icon name="delete" color={color.error} onPress={() => {confirmAndDeleteFolder(item.id)}}></Icon>
      <ListItem.Chevron size={24} color={color.palette.blue} onPress={() => { navigateToRecords(item) }} />
    </ListItem>
    
  );

  return (
    <View style={Styles.root}>  
      <GradientBackground colors={[color.palette.lighterGrey, color.palette.lighterGrey]} />
      <Screen preset="fixed" style={Styles.screen} backgroundColor={color.transparent}>
        <FlatList data={folders.slice()} keyExtractor={item => item.id} renderItem={renderItem} style={{flex:1}} />
        <FAB
          onPress={() => showFolderOverlay({id: 0, name: ""})}
          placement="right"
          color={color.primary}
          icon={{ name: 'add', color: 'white' }}
        />
        <Overlay isVisible={isAddFolderFormVisible} overlayStyle={Styles.addFolderContainer}>
            <View >
              <Input
                ref={newFolderInput}
                textAlign="left"
                value={folderToUpdate.name}
                containerStyle={{}}
                inputContainerStyle={{ }}
                errorMessage={folderToUpdate.error}
                errorStyle={{}}
                errorProps={{}}
                inputStyle={{}}
                label="New Folder"
                labelStyle={{}}
                labelProps={{}}
                leftIcon={<Icon name="folder" size={20} color={color.primary}/>}
                leftIconContainerStyle={{}}
                rightIconContainerStyle={{}}
                onChangeText={text => validateAndSetNewFolderName(text)}
                placeholder="Enter folder name"
              />
              <View style={{alignSelf: "flex-end", flexDirection: "row"}}>
                <Button
                  buttonStyle={{ }}
                  containerStyle={{ }}
                  disabledStyle={{}}
                  disabledTitleStyle={{}}
                  linearGradientProps={null}
                  loadingProps={{ animating: true }}
                  loadingStyle={{}}
                  onPress={() => setIsAddFolderFormVisible(false)}
                  title="Cancel"
                  titleProps={{}}
                  titleStyle={{  color:color.palette.lightGrey }}
                  type="clear"
                />
                {folderToUpdate.id == 0 && <Button
                  buttonStyle={{  }}
                  containerStyle={{ }}
                  disabledStyle={{}}
                  disabledTitleStyle={{}}
                  linearGradientProps={null}
                  loadingProps={{ animating: true }}
                  loadingStyle={{}}
                  onPress={() => createNewFolder()}
                  title="Create"
                  titleProps={{}}
                  titleStyle={{  }}
                  type="clear"
                />}
                {folderToUpdate.id != 0 && <Button
                  buttonStyle={{  }}
                  containerStyle={{ }}
                  disabledStyle={{}}
                  disabledTitleStyle={{}}
                  linearGradientProps={null}
                  loadingProps={{ animating: true }}
                  loadingStyle={{}}
                  onPress={() => updateFolder()}
                  title="Update"
                  titleProps={{}}
                  titleStyle={{  }}
                  type="clear"
                />}
              </View>
            </View>
        </Overlay>
      </Screen>
    </View>
  )
})
