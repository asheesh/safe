import React, { FC } from "react"
import { observer } from "mobx-react-lite"
import { StyleSheet, Text, View } from "react-native"
import { Screen } from "../../components"
import { color } from "../../theme"
import { Button, Icon, Input } from "react-native-elements"
import { Controller, useForm } from "react-hook-form"
import EncryptedStorage from 'react-native-encrypted-storage';
import { StackScreenProps } from "@react-navigation/stack"
import { NavigatorParamList } from "../../navigators"
import Toast from "react-native-root-toast"
import DeviceInfo from 'react-native-device-info';

const Styles = StyleSheet.create({
  root:{
    flex: 1,
    justifyContent: "space-evenly", 
     paddingVertical: 10,
     paddingHorizontal: 25
  },
  logo:{
    color:color.primary
  },
  header:{
    color: color.palette.blue, 
    fontSize: 50, 
    marginLeft: 5
  }
})

export const SignupScreen : FC<StackScreenProps<NavigatorParamList, "signup">> = observer(({ navigation }) => {
  const { control, handleSubmit, formState: { errors } } = useForm();
  const onSubmit = data => {
    if(data.secureKey != data.confirmSecureKey){
      Toast.show('Pin does not match', {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0
      });
      return
    }
    EncryptedStorage.setItem("name", data.name).then(() => {
      EncryptedStorage.setItem("pass_key", data.secureKey).then(() => {
        navigation.navigate("home")
      })
    })
  }

  return (
    <Screen style={Styles.root} preset="fixed">
      <View style={{  alignItems: "center", justifyContent: "center"}}>
        <Icon name="safe" type="material-community" size={60}  color={color.primary}/>
        <View style={{ justifyContent: "space-around", alignItems: "center"}}>
          <Text style={Styles.header}>Safe</Text>
          <Text style={{color: color.palette.blue, marginStart: 70, marginTop: -10}}>{"v" + DeviceInfo.getVersion()}</Text>
        </View>
      </View>
      <View>
      <View>
          <Controller
              control={control}
              rules={{
              required: true,
              }}
              render={({ field: { onChange, onBlur, value } }) => (
                <Input
                  errorMessage={errors.Name && "This is required."}
                  label="Name"
                  leftIcon={<Icon name="tag" size={20} color={color.primary} type="font-awesome" />}
                  onBlur={onBlur}
                  onChangeText={onChange}
                  value={value} textAlign="left" />
              )}
              name="name"
              defaultValue=""
            />
            <Controller
              control={control}
              rules={{
              required: true,
              }}
              render={({ field: { onChange, onBlur, value } }) => (
                <Input secureTextEntry={true}
                  errorMessage={errors.secureKey && "This is required."}
                  label="Master Pin"
                  leftIcon={<Icon name="key" size={20} color={color.primary} type="font-awesome" />}
                  onBlur={onBlur}
                  onChangeText={onChange}
                  value={value} textAlign="left" />
              )}
              name="secureKey"
              defaultValue=""
            />
            <Controller
              control={control}
              rules={{
              required: true,
              }}
              render={({ field: { onChange, onBlur, value } }) => (
                <Input secureTextEntry={true}
                  errorMessage={errors.confirmSecureKey && "This is required."}
                  label="Confirm Master Pin"
                  leftIcon={<Icon name="key" size={20} color={color.primary} type="font-awesome" />}
                  onBlur={onBlur}
                  onChangeText={onChange}
                  value={value} textAlign="left" />
              )}
              name="confirmSecureKey"
              defaultValue=""
            />
            <Button onPress={handleSubmit(onSubmit)} title="Confirm" />
        </View>
      </View>
    </Screen>
  )
})
