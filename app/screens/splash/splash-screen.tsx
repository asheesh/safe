import React from "react"
import { observer } from "mobx-react-lite"
import { Text, StyleSheet } from "react-native"
import { Screen } from "../../components"
import { color } from "../../theme"
import { Icon } from "react-native-elements"

const Styles = StyleSheet.create({
  root:{
    flex: 1,
    justifyContent: "center", 
    alignItems: "center"
  },
  logo:{
    color:color.primary
  },
  header:{
    fontSize: 98,
    color: color.palette.blue,
    marginBottom:"20%"
  }
})

export const SplashScreen = observer(function SplashScreen() {
  return (
    <Screen style={Styles.root} preset="fixed">
      <Icon name="safe" type="material-community" size={170}  color={color.primary}/>
      <Text style={Styles.header}>Safe</Text>
    </Screen>
  )
})
