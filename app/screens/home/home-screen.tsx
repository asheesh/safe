import React, { FC, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { Text, StyleSheet, Image, TouchableOpacity, View } from "react-native"
import { Screen } from "../../components"
import { color } from "../../theme"
import { LoggedUserModel, useStores } from "../../models"
import FingerprintScanner from 'react-native-fingerprint-scanner';
import { Button, Icon, Input } from "react-native-elements"
import * as RNFS from 'react-native-fs';
import { virgilCrypto } from 'react-native-virgil-crypto';
import EncryptedStorage from 'react-native-encrypted-storage';
import { Controller, useForm } from "react-hook-form"
import { StackScreenProps } from "@react-navigation/stack"
import { NavigatorParamList } from "../../navigators"
import Toast from "react-native-root-toast"
import DeviceInfo from 'react-native-device-info';

const Styles = StyleSheet.create({
  root:{
    flex: 1,
    justifyContent: "center", 
    paddingHorizontal: 25
  },
  logo:{
    color:color.primary
  },
  header:{
    fontSize: 78,
    color: color.palette.blue,
  },
  fingerprint:{
    alignSelf: "center",
    maxWidth: "100%",
    width: 150,
    height: 150,
  }
})

export const HomeScreen : FC<StackScreenProps<NavigatorParamList, "home">> = observer(({ navigation }) => {
  const { loggedUser, folderStore, recordStore } = useStores()
  const { control, handleSubmit, formState: { errors } } = useForm();
  const [secureKey, setSecureKey] = useState(null)
  const [useBiometrics, setUseBiometrics] = useState(false)

  useEffect(() => {
    EncryptedStorage.getItem("pass_key").then((passKey) => {
      setSecureKey(passKey)
    });
    EncryptedStorage.getItem("use_biometrics").then((useBiometrics) => {
      setUseBiometrics(useBiometrics  == "true" ? true : false)
    });
  }, [])
  
  const onSubmit = data => {
    if(data.secureKey == secureKey){
      doLogin()
    }else{
      Toast.show('Incorrect pin', {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0
      });
    }
  }

  const doLogin = () => {
    var path = RNFS.DocumentDirectoryPath + "/safedata"
    RNFS.readFile(path, 'utf8').then(async res => {
      var key = await EncryptedStorage.getItem("secure_key_private");
      const privateKey = virgilCrypto.importPrivateKey(key);
      const decryptedData = virgilCrypto.decrypt(res, privateKey);
      var data = JSON.parse(decryptedData.toString('utf8'));
      folderStore.saveFolders(data.folderStore.folders)
      recordStore.saveRecords(data.recordStore.records)
      loggedUser.saveLoggedUser(LoggedUserModel.create({token:"fake_token"}))
    })
    .catch(err => {
      if(err.message.indexOf("No such file or directory") >= 0){
        loggedUser.saveLoggedUser(LoggedUserModel.create({token:"fake_token"}))
      }
        console.log(err.message, err.code);
    });
  }

  // Pull in navigation via hook
  const showAuthenticationDialog = () => {
    FingerprintScanner
    .isSensorAvailable()
    .then(biometryType => {
      if(biometryType != null){
        FingerprintScanner.authenticate({
          title: 'Scan your Fingerprint on the device scanner to continue'
        })
          .then(() => {
            doLogin()
          })
          .catch((error) => {
            console.log('Authentication error is => ' + error);
          })
      }else{
        console.log('biometric authentication is not available')
      }
    })
    .catch(error => {
      console.log('biometric authentication is not available')
    });
  }

  useEffect(() => {

    return () => {
      FingerprintScanner.release();
    };
  });

  return (
    <Screen style={Styles.root} preset="fixed">
      <View style={{marginTop:40}}>
        <Icon name="safe" type="material-community" size={100}  color={color.primary}/>
        <View style={{ justifyContent: "space-around", alignItems: "center"}}>
          <Text style={Styles.header}>Safe</Text>
          <Text style={{color: color.palette.blue, marginStart: 110, marginTop: -15}}>{"v" + DeviceInfo.getVersion()}</Text>
        </View>
        
      </View>
      
      <View style={{ flexGrow:1, justifyContent: "space-evenly"}}>
        <View style={{paddingHorizontal:20}}>
          <Controller
              control={control}
              rules={{
              required: true,
              }}
              render={({ field: { onChange, onBlur, value } }) => (
                <Input secureTextEntry={true}
                  errorMessage={errors.secureKey && "This is required."}
                  label="Master Pin"
                  leftIcon={<Icon name="key" size={20} color={color.primary} type="font-awesome" />}
                  onBlur={onBlur}
                  onChangeText={onChange}
                  value={value} textAlign="left" />
              )}
              name="secureKey"
              defaultValue=""
            />
            <Button onPress={handleSubmit(onSubmit)} title="Confirm" />
        </View>
        {useBiometrics && <View style={{borderColor: color.primary, borderWidth: 1}}></View>}
        {
          useBiometrics &&
          <TouchableOpacity  onPress={showAuthenticationDialog}>
            <Image source={require("./fingerprint.jpeg")} style={Styles.fingerprint} />
          </TouchableOpacity>
        }
        
      </View>
    </Screen>
  )
})
