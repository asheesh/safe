import React, { FC, useEffect } from "react"
import { observer } from "mobx-react-lite"
import { Alert, FlatList, StyleSheet, View } from "react-native"
import { GradientBackground, Screen } from "../../components"
import { color } from "../../theme"
import { StackScreenProps } from "@react-navigation/stack"
import { NavigatorParamList } from "../../navigators"
import { Button, FAB, Icon, ListItem } from "react-native-elements"
import { useStores } from "../../models"
import { TouchableOpacity } from "react-native-gesture-handler"
import Toast from "react-native-root-toast"

const styles = StyleSheet.create({
  root:{
    flex: 1,
    padding:5
  },
  screen:{
    backgroundColor: color.transparent,
    elevation:10
  },
})

export const RecordScreen: FC<StackScreenProps<NavigatorParamList, "records">> = observer(({ route, navigation }) => {

  const { recordStore } = useStores()

  useEffect(() => {
    navigation.setOptions({
      title: route.params.folderName,
    })
  }, [])

  const confirmAndDeleteRecord = (recordId) => {
    Alert.alert(
      "Confirm",
      "Are you sure?",
      [
        {
          text: "Cancel",
          style: "cancel"
        },
        { text: "OK", onPress: () => deleteRecord(recordId) }
      ]
    );
  }

  const deleteRecord = (recordId) => {
    recordStore.deleteRecord(recordId)
    Toast.show('Deleted', {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0
    });
  }
  

  const renderItem = ({ item }) => (
    <ListItem bottomDivider topDivider key={item.id}>
      <ListItem.Content>
        <ListItem.Title>{ item.name }</ListItem.Title>
      </ListItem.Content>
      <Icon name="delete" color={color.error} onPress={() => {confirmAndDeleteRecord(item.id)}}></Icon>
      <ListItem.Chevron size={24} color={color.palette.blue} onPress={() => { navigation.navigate("addRecord", {folderId: route.params.folderId, folderName: route.params.folderName, recordId: item.id}); }} />
    </ListItem>
  );

  return (
    <View style={styles.root}>  
      <GradientBackground colors={[color.palette.lighterGrey, color.palette.lighterGrey]} />
      <Screen preset="fixed" style={styles.screen} backgroundColor={color.transparent}>
        <FlatList data={recordStore.findByFolderId(route.params.folderId)} keyExtractor={item => item.id} renderItem={renderItem} style={{flex:1} }/>
        <FAB
          onPress={() => {navigation.navigate("addRecord", {folderId: route.params.folderId, folderName: route.params.folderName, recordId: 0});}}
          placement="right"
          color={color.primary}
          icon={{ name: 'add', color: 'white' }}
        />
      </Screen>
    </View>
  )
})
